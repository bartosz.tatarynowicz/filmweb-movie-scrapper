
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


/**
 *
 * @author kel
 */
public class FilmwebMovieScrapper {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        Document doc = null;
        try
        {
            doc = Jsoup.connect("https://www.filmweb.pl/premiere/2021/7").get();
        } catch (IOException ex)
        {
            Logger.getLogger(FilmwebMovieScrapper.class.getName()).log(Level.SEVERE, null, ex);
        }
        String pageTitle = doc.title();
        System.out.println(pageTitle);
        Elements elements = doc.getElementsByClass("premieresList__boxes");
        Elements titles = doc.getElementsByClass("filmPreview__title");
        Document subdoc = null;
        for (Element element: elements)
        {
            subdoc = Jsoup.parseBodyFragment(element.outerHtml());
            titles = subdoc.getElementsByClass("filmPreview__title");
            Elements years = subdoc.getElementsByClass("filmPreview__year");
            Elements releases = subdoc.getElementsByClass("filmPreview__release");
            Elements directors = subdoc.getElementsByClass("filmPreview__info filmPreview__info--directors");
            Elements links = subdoc.getElementsByClass("poster__link");
            int i = 0;
            for (Element title : titles)
            {
                System.out.println(title.text());                
                System.out.println("Rok produkcji: "+years.get(i).text());            
                System.out.println("Data premiery: "+releases.get(i).attr("data-release"));
                System.out.println("Link: https://filmweb.pl"+links.get(i).attr("href"));
                // let's fetch more data for this movie
                // first - description (if exists)
                Document movie = null;
                try
                {
                    movie = Jsoup.connect("https://filmweb.pl"+links.get(i).attr("href")+"/descs").get();
                } catch (IOException ex)
                {
                    Logger.getLogger(FilmwebMovieScrapper.class.getName()).log(Level.SEVERE, null, ex);
                }
                Elements movieElements = movie.getElementsByClass("descriptionSection__text descriptionSection__text--full");                    
                if(movieElements.size()>0)
                {
                    System.out.println("Opis: "+movieElements.get(0).text());
                } 
                else
                {
                try
                {
                    movie = Jsoup.connect("https://filmweb.pl"+links.get(i).attr("href")).get();
                } catch (IOException ex)
                {
                    Logger.getLogger(FilmwebMovieScrapper.class.getName()).log(Level.SEVERE, null, ex);
                }
                movieElements = movie.getElementsByClass("filmPosterSection__plot clamped");
                if(movieElements.size()>0)
                {
                    System.out.println("Opis: "+movieElements.get(0).text());
                }
                }

                 // now genre
                try
                {
                    movie = Jsoup.connect("https://filmweb.pl"+links.get(i).attr("href")).get();
                } catch (IOException ex)
                {
                    Logger.getLogger(FilmwebMovieScrapper.class.getName()).log(Level.SEVERE, null, ex);
                }
                movieElements = movie.getElementsByAttributeValue("itemprop", "genre");
                if(movieElements.size()>0)
                {
                    System.out.println("Gatunek: "+movieElements.get(0).text());
                }
               System.out.println("-----");
               i++;          
            }
        }
        
    }
    
}
