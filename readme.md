# Filmweb Movie Scrapper

Filmweb Movie Scrapper is a simple data scrapper class written in Java with [jsoup](https://jsoup.org/) library usage. Class grabs movie premieres from filmweb.pl site with some additional data, and display it.

## Usage

Import files as project in NetBeans IDE, or simply run FilmwebMovieScrapper.java class.
